<?php 

// Takes raw data from the request
$json = file_get_contents('php://input');

// Converts it into a PHP object
$data = json_decode($json, true);
$datafile    = file_get_contents("data.json");
$stored_data = json_decode($datafile, true);
array_push( $stored_data['items'], $data );
$output    = fopen("data.json", "w") or die("Unable to open file!");
fwrite($output, json_encode( $stored_data ));
fclose($output);
