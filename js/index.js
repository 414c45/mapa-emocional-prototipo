/**
 *   Main JS File
 */

var sample_conf = {
    params : [
        {
            name : 'Industrial',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Orgánico',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Floral',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Leñoso',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Frutal',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Químico',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Mentolado',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Dulce',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Quemado',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Cítrico',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Rancio',
            min  : 0,
            max  : 100,
            default : 0,
        },
        {
            name : 'Descompuesto',
            min  : 0,
            max  : 100,
            default : 0,
        }
    ]
};

//var ws = new WebSocket("wss://0.0.0.0:12345/socket.php");
var server_url = '/receiver.php';
var map_container = document.querySelector('#emotional-map');
var form          = document.querySelector('.form-emotional');
var recording     = false;
var watcher;

document.addEventListener('DOMContentLoaded', ()=>{

    /**
     *   Set up map
     */

    var marker;

    var map = L.map('emotional-map', {
        zoomControl: false,
        attributionControl: false
    }).setView([
        38.4465652,
        -6.3600776
    ], 16);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);


    /**
     *   Set up map actions
     */
    var map_actions = document.querySelectorAll('.site-map__action');
    map_actions.forEach( button => {
        button.addEventListener('click', e => {
            switch( button.dataset.action ){
                case 'record':
                    if(!recording){
                        recording = true;
                        button.classList.add('active');
                        startRecording();
                    } else {
                        recording = false;
                        stopRecording();
                        button.classList.remove('active')
                    }
                break;
            }
        });
    });

    startRecording = () => {
        watcher = navigator.geolocation.watchPosition( sendData, showErrors, {
            maximumAge          : 0,
            timeout             : 5000,
            enableHighAccuracy  : true
        });
    };

    stopRecording = () => {
        navigator.geolocation.clearWatch(watcher);
    }

    sendData = (data) => {
        var coords = [
            data.coords.latitude,
            data.coords.longitude
        ];
        // If marker is not created yet
        if(!marker){
            marker = L.marker(coords).addTo(map);
            map.setView(coords);
        } else {
            // Move marker
            marker.setLatLng(coords);
            map.setView(coords);
            // Send data
            var inputs = [...document.querySelectorAll('.form-param input')];
            var item = {
                lat : coords[0],
                lon : coords[1],
                val : inputs.map(i => { 
                    return {
                        'name' : i.name,
                        'val'  : i.value
                    }
                }),
                datetime: new Date(),
            }
            //ws.send(data);
            fetch( server_url, {
                method : "post",
                headers : {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body : JSON.stringify(item)
            }).then( response => response.json).then( data => { console.log(data); });
        }
    };
    showErrors = (e) => {
        console.log(e);
    };

    /**
     *    Set up navigation
     */
    var triggers = document.querySelectorAll('[data-show]');
    triggers.forEach( trigger => {
        trigger.addEventListener('click', ()=>{
            document.querySelector('.site-navigation__item.active').classList.remove('active');
            trigger.classList.add('active');
            document.querySelector('section.active').classList.remove('active');
            var target = trigger.dataset.show;
            document.querySelector('[data-id="'+target+'"]').classList.add('active');
        })
    });

    /**
     *    Set up form
     */
    sample_conf.params.forEach( i => {
        var wrapper = document.createElement('div');
        wrapper.classList.add('form-param');
        var label = document.createElement('label');
        label.innerHTML = i.name;
        var input  = document.createElement('input');
        input.type  = 'range';
        input.min   = i.min;
        input.max   = i.max;
        input.name  = i.name;
        input.value = i.default;
        wrapper.appendChild(label);
        wrapper.appendChild(input);
        form.appendChild(wrapper);
    });

});
