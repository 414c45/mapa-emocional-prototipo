<?php 

$datafile = file_get_contents('data.json');
$data     = json_decode( $datafile, true );
header('Content-Type: application/json; charset=utf-8');
echo json_encode( $data, JSON_PRETTY_PRINT );

